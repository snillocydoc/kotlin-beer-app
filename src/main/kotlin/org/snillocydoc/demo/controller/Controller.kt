package org.snillocydoc.demo.controller

import org.snillocydoc.demo.repository.BeerRepository
import org.snillocydoc.demo.model.Beer
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class Controller(private val beerRepository: BeerRepository) {

    @GetMapping("/beers")
    fun beers(): List<Beer> {
        return beerRepository.findAll()
    }

    @PostMapping("/beers")
    fun createBeer(@Valid @RequestBody beer: Beer): Beer {
        return beerRepository.save(beer)
    }

}