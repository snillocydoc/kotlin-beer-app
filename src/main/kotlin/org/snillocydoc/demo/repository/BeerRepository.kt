package org.snillocydoc.demo.repository

import org.snillocydoc.demo.model.Beer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BeerRepository : JpaRepository<Beer, Long>