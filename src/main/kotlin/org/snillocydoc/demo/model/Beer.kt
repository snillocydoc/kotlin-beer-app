package org.snillocydoc.demo.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity
data class Beer (
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val brand: String,
    val calories: Int,
    val abv: Double
)
